﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TwoFactorAuthenticator.Misc;

namespace TwoFactorAuthenticator.Controllers
{
    [Route("v1/Google")]
    public class GoogleController : Controller
    {
        /// <summary>
        /// Get Google 2Factor Authenticator Code
        /// </summary>
        /// <param name="secret">Authenticator Secret</param>
        /// <returns>String</returns>
        [HttpGet("Get")]
        public IActionResult Get(string secret)
        {
            var bytes = Base32Encoding.ToBytes(secret);

            var totp = new Totp(bytes);

            return Ok(totp.ComputeTotp());
        }
    }
}